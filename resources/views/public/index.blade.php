@php($isSiteGroup = $grupo->site_group && $grupo->is($grupo->site_group))

@extends('layouts.app')

@section('content')
    <div class="group-page__container" >

        <h1 class="group-page__title">
            Testemunhos
            @unless($isSiteGroup)
                <small class="group-page__title__group">
                    {{ $grupo->nome_curto }}
                </small>
            @endunless
        </h1>

        @if($collections->isNotEmpty())

            <section class="s-tst-grid md:s-tst-grid-cols-2 lg:s-tst-grid-cols-3 s-tst-gap-4">
                @foreach($collections as $collection)
                    <article class="s-tst-bg-white s-tst-rounded-lg s-tst-shadow s-tst-flex s-tst-flex-col s-tst-overflow-hidden">
                        <div
                                class="s-tst-aspect-[2/1] s-tst-bg-cover s-tst-bg-center s-tst-relative @if($collection->getMedia('banner')->isEmpty()) s-tst-bg-gray-200 s-tst-text-gray-400 s-tst-text-6xl s-tst-flex s-tst-justify-center s-tst-items-center @endif"
                                @if($collection->getMedia('banner')->isNotEmpty()) style="background-image: url({{$collection->getFirstMediaUrl('banner')}});" @endif
                        >
                            @if($collection->getMedia('banner')->isEmpty())
                                @if(isset($grupo) and ($grupo->icon ?? $grupo->logo))
                                    <img src="{{ $grupo->icon ?? $grupo->logo }}" class="s-tst-w-auto s-tst-h-auto s-tst-object-contain s-tst-max-w-[50%] s-tst-max-h-[70%] s-tst-grayscale s-tst-opacity-[15%]">
                                @else
                                    <x-icon icon="conversation" class="s-tst-w-1/3 s-tst-h-3/4 s-tst-fill-current" />
                                @endif
                            @endif
                        </div>
                        <div class="s-tst-px-4 s-tst-py-3 s-tst-flex-1 s-tst-flex s-tst-flex-col">
                            <h3 class="s-tst-font-headline s-tst-text-xl">
                                <a href="{{ $grupo->route('testimonials.public.create', $collection) }}" class="s-tst-text-primary-500 hover:s-tst-text-primary-600">{{ $collection->name }}</a>
                            </h3>
                        </div>
                    </article>
                @endforeach
            </section>

        @else

            <section class="sautor-list--empty">
                Não existem coleções neste grupo.
            </section>

        @endif
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/testimonials/styles.css') }}">
@endpush
