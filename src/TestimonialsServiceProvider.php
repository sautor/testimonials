<?php

namespace Sautor\Testimonials;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Services\Addons\Addon;
use Sautor\Testimonials\Livewire\TestimonialForm;
use Sautor\Testimonials\Models\Testimonial;
use Sautor\Testimonials\Models\TestimonialCollection;
use Sautor\Testimonials\Policies\CollectionsPolicy;
use Sautor\Testimonials\Policies\TestimonialsPolicy;
use Spatie\Translatable\Facades\Translatable;

class TestimonialsServiceProvider extends ServiceProvider
{
    protected $policies = [
        TestimonialCollection::class => CollectionsPolicy::class,
        Testimonial::class => TestimonialsPolicy::class,
    ];

    private Addon $addon;

    /**
     * PaymentsServiceProvider constructor.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->addon = Addon::create('testimonials', 'Testemunhos', 'comment-lines', 'Extensão para recolher e gerir testemunhos.')
            ->setGroupAddon(true)
            ->setManagersOnly(false)
            ->setEntryRouteForGroup('testimonials.public.index')
            ->setEntryRouteForGroupAdmin('testimonials.collections.index')
            ->setRestrict(function (?Grupo $grupo) {
                return ! TestimonialCollection::where('grupo_id', $grupo->id)->open()->exists();
            })
            ->withAssets();
    }

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        $this->loadTranslationsFrom(__DIR__.'/../lang', 'testimonials');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'testimonials');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        $this->registerPolicies();

        $addonService = $this->app->make('AddonService');
        $addonService->register($this->addon);

        Livewire::component('testimonial-form', TestimonialForm::class);

        Translatable::fallback(
            fallbackLocale: 'fr',
            fallbackAny: true
        );
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        //
    }

    public function registerPolicies()
    {
        foreach ($this->policies as $key => $value) {
            Gate::policy($key, $value);
        }
    }
}
