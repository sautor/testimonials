@php($isSiteGroup = $grupo->site_group && $grupo->is($grupo->site_group))

@extends('layouts.app')

@section('content')
    @php($hero = $collection->getFirstMediaUrl('banner'))
    @if($hero)
        <div class="hero @if($grupo->site_group) hero--group @endif ">
            <div class="hero__image" style="background-image: url('{{ $hero }}')">
            </div>
        </div>
    @endif

    <div class="group-page__container" >

        <livewire:testimonial-form :collection="$collection" />

    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/testimonials/styles.css') }}">
@endpush
