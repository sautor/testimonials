@unless($testimonials->isEmpty())
    <section class="sautor-list sheets-list">
        @foreach($testimonials as $testimonial)
            <div class="sautor-list__item">
                <div class="sautor-list__item__logo">
                    <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
                        <span class="fad {{ $testimonial->hasMedia('audio') ? 'fa-comment-music' : 'fa-comment-lines' }}"></span>
                    </span>
                </div>
                <a href="{{ $testimonial->collection->grupo->route('testimonials.testimonials.show', $testimonial) }}" class="sautor-list__item__data">
                    <p class="sautor-list__item__name">
                        {{ $testimonial->name }}
                    </p>
                    <p class="sautor-list__item__meta">
                        {{ $testimonial->origin }}

                        @if($testimonial->tagsWithType($testimonial->collection->tagType)->isNotEmpty())
                            &middot; <span class="far fa-tag"></span> {{ $testimonial->tagsWithType($testimonial->collection->tagType)->count() }}
                        @endif

                        @if($testimonial->hasMedia('images'))
                            &middot; <span class="far fa-images"></span> {{ $testimonial->getMedia('images')->count() }}
                        @endif

                        @if($testimonial->reviewer)
                            &middot; <span class="s-tst-text-primary-600">A ser revisto por {{ $testimonial->reviewer->nome_curto }}</span>
                        @endif
                    </p>
                </a>

                <div class="sautor-list__item__actions">
                    @unless(empty($actions) or empty($a = $actions($collection)))
                        @foreach($a as $action)
                            <a title="{{ $action['label'] }}"
                               @if(!empty($action['link']))
                                   href="{{ $action['link'] }}"
                               @elseif(!empty($action['modal']))
                                   href="#" data-toggle="modal" data-target="#{{ $action['modal'] }}"
                                    @endif
                            >
                                <span class="{{ $action['icon'] }}"></span>
                            </a>
                        @endforeach
                    @endunless
                    <a title="Ver" href="{{ $testimonial->collection->grupo->route('testimonials.testimonials.show', $testimonial) }}">
                        <span class="far fa-angle-right"></span>
                    </a>
                </div>
            </div>
        @endforeach
    </section>
@else
    <section class="sautor-list--empty tags-list--empty">
        {{ empty($empty_message) ? 'Não existem testemunhos.' : $empty_message }}
    </section>
@endunless
