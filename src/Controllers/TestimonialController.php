<?php

namespace Sautor\Testimonials\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;
use Sautor\Testimonials\Models\Testimonial;

class TestimonialController extends Controller
{
    public function show(Grupo $grupo, Testimonial $testimonial)
    {
        $this->authorize('view', $testimonial);
        return view('testimonials::testimonials.show', compact('grupo', 'testimonial'));
    }

    public function edit(Grupo $grupo, Testimonial $testimonial)
    {
        $this->authorize('update', $testimonial);
        return view('testimonials::testimonials.edit', compact('grupo', 'testimonial'));
    }

    public function update(Request $request, Grupo $grupo, Testimonial $testimonial) {
        $this->authorize('update', $testimonial);
        $testimonial->update($request->only(['reviewer_id', 'reviewed_content', 'notes']));

        Notification::make()
            ->title('Testemunho editada com sucesso.')
            ->success()
            ->send();

        return redirect($grupo->route('testimonials.testimonials.show', $testimonial));
    }
}
