<?php

namespace Sautor\Testimonials\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\Core\Models\Pessoa;
use Sautor\Testimonials\Models\TestimonialCollection;

class CollectionsPolicy
{
    use HandlesAuthorization;

    public function viewAny(?Pessoa $user)
    {
        return true;
    }

    public function view(?Pessoa $user, TestimonialCollection $collection)
    {
        return $collection->isOpen || $collection->grupo->isManagedBy($user);
    }

    public function create(Pessoa $user)
    {
        return $user->grupos()->where('inscricoes.responsavel', true)->exists();
    }

    public function update(Pessoa $user, TestimonialCollection $collection)
    {
        return $collection->grupo->isManagedBy($user);
    }

    public function delete(Pessoa $user, TestimonialCollection $collection)
    {
        return $collection->grupo->isManagedBy($user);
    }

    public function restore(Pessoa $user, TestimonialCollection $collection)
    {
        return $collection->grupo->isManagedBy($user);
    }

    public function forceDelete(Pessoa $user, TestimonialCollection $collection)
    {
        return false;
    }
}
