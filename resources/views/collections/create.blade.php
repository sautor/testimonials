@extends('layouts.group-admin')

@section('group-content')
    <header class="group-page__header">
        <h1 class="group-page__header__title">
            Criar coleção de testemunhos
        </h1>
    </header>

    @php($scope = 'new-collection')
    <form action="{{ $grupo->route('testimonials.collections.store') }}" method="POST" enctype="multipart/form-data" data-vv-scope="{{ $scope }}">
        @csrf

        @include('testimonials::collections.form')

        <div class="sautor-addon__actions">
            <button type="submit" class="button button--primary">Criar</button>
        </div>
    </form>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/testimonials/styles.css') }}">
@endpush
