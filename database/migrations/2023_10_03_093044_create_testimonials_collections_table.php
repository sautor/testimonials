<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('tst_testimonials_collections', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->dateTime('open_at')->nullable();
            $table->dateTime('close_at')->nullable();
            $table->longText('opening_text')->nullable();
            $table->longText('disclaimer')->nullable();
            $table->longText('thankyou_text')->nullable();
            $table->boolean('accept_audio')->default(true);
            $table->boolean('accept_images')->default(true);
            $table->string('tags')->nullable();
            $table->unsignedInteger('grupo_id')->nullable();
            $table->foreign('grupo_id')->references('id')->on('grupos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tst_testimonials_collections');
    }
};
