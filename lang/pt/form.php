<?php

return [
    'step' => 'Passo :idx',
    'success' => 'Testemunho enviado com sucesso.',
    'loading' => 'A carregar...',
    'start' => 'Começar',
    'personal_data' => 'Dados pessoais',
    'testimonial' => 'Testemunho',
    'images' => 'Fotografias',
    'consent' => 'Consentimento',
    'name' => 'Nome',
    'contact_message' => 'Os campos de contacto abaixo são opcionais e apenas serão usados em caso de necessidade.',
    'email' => 'Endereço de e-mail',
    'phone' => 'N.º de telefone',
    'origin' => 'Paróquia e/ou grupos a que pertence',
    'tags_label' => 'Escolha todos os que se apliquem',
    'next' => 'Seguinte',
    'testimonial_text' => 'Testemunho em texto',
    'testimonial_audio' => 'Testemunho em áudio',
    'content' => 'Conteúdo do testemunho',
    'audio' => 'Ficheiro áudio',
    'consent_message' => 'Eu dou o meu consentimento ao uso deste testemunho por :group de :app.',
    'send' => 'Enviar',
    'sending' => 'A enviar...',
    'error' => [
        'title' => 'Ocorreu um erro',
        'body' => 'Por favor tente novamente.'
    ]
];
