<?php

namespace Sautor\Testimonials\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mtvs\EloquentHashids\HasHashid;
use Mtvs\EloquentHashids\HashidRouting;
use Sautor\Core\Models\Grupo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Translatable\HasTranslations;

class TestimonialCollection extends Model implements HasMedia
{
    use HasHashid, HashidRouting;
    use InteractsWithMedia;
    use SoftDeletes;
    use HasTranslations;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tst_testimonials_collections';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'open_at' => 'datetime',
        'close_at' => 'datetime',
        'accept_audio' => 'boolean',
        'accept_images' => 'boolean',
        'languages' => 'array',
    ];

    public $translatable = ['name', 'opening_text', 'disclaimer', 'thankyou_text'];

    protected function isOpen(): Attribute
    {
        return Attribute::make(
            get: fn () => ($this->open_at && $this->open_at < Carbon::now()) && (! $this->close_at || $this->close_at > Carbon::now())
        );
    }

    protected function isClosed(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->close_at && $this->close_at < Carbon::now()
        );
    }

    public function grupo(): BelongsTo
    {
        return $this->belongsTo(Grupo::class);
    }

    public function testimonials(): HasMany
    {
        return $this->hasMany(Testimonial::class);
    }

    public function scopeOpen(Builder $query): void
    {
        $query->where('open_at', '<', Carbon::now())->where(function (Builder $qb) {
            $qb->whereNull('close_at')->orWhere('close_at', '>', Carbon::now());
        });
    }

    public function scopeNotOpen(Builder $query): void
    {
        $query->whereNull('open_at');
    }

    public function scopeClosed(Builder $query): void
    {
        $query->where('close_at', '<', Carbon::now());
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('banner')->singleFile();
    }

    protected function tagType(): Attribute
    {
        return Attribute::make(
            get: fn () => 'tst-collection-'.$this->id
        );
    }
}
