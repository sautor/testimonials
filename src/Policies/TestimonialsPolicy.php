<?php

namespace Sautor\Testimonials\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\Core\Models\Pessoa;
use Sautor\Testimonials\Models\Testimonial;

class TestimonialsPolicy
{
    use HandlesAuthorization;

    public function viewAny(?Pessoa $user)
    {
        //
    }

    public function view(Pessoa $user, Testimonial $testimonial)
    {
        return $user->can('view', $testimonial->collection);
    }

    public function create(?Pessoa $user)
    {
        return true;
    }

    public function update(Pessoa $user, Testimonial $testimonial)
    {
        return $user->can('update', $testimonial->collection);
    }

    public function delete(Pessoa $user, Testimonial $testimonial)
    {
        return $user->can('update', $testimonial->collection);
    }

    public function restore(Pessoa $user, Testimonial $testimonial)
    {
        return $user->can('update', $testimonial->collection);
    }

    public function forceDelete(Pessoa $user, Testimonial $testimonial)
    {
        return false;
    }
}
