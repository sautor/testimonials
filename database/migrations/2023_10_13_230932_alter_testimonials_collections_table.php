<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Sautor\Testimonials\Models\TestimonialCollection;

return new class extends Migration {
    const FIELDS = ['name', 'opening_text', 'disclaimer', 'thankyou_text'];

    public function up(): void
    {
        TestimonialCollection::all()->each(function($tst) {
            foreach(self::FIELDS as $attr) {
                echo $tst->id;
                echo $attr;
                var_dump(json_encode(['pt' => $tst->$attr]));
                if (!empty($tst->getRawOriginal($attr))) $tst->$attr = ['pt' => $tst->getRawOriginal($attr)];
            }
            $tst->save();
        });

        Schema::table('tst_testimonials_collections', function (Blueprint $table) {
            foreach(self::FIELDS as $attr) {
                $table->json($attr)->nullable()->change();
            }

            $table->json('languages')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('tst_testimonials_collections', function (Blueprint $table) {
            foreach(self::FIELDS as $attr) {
                $table->text($attr)->nullable()->change();
            }

            $table->dropColumn('languages');
        });

        TestimonialCollection::all()->each(function($tst) {
            foreach(self::FIELDS as $attr) {
                if (!empty($tst->$attr)) $tst->setRawAttributes([$attr => $tst->attr]);
            }
            $tst->save();
        });
    }
};
