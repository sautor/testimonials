<?php

namespace Sautor\Testimonials\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mtvs\EloquentHashids\HasHashid;
use Mtvs\EloquentHashids\HashidRouting;
use Propaganistas\LaravelPhone\Casts\RawPhoneNumberCast;
use Sautor\Core\Models\Pessoa;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Tags\HasTags;

class Testimonial extends Model implements HasMedia
{
    use HasHashid, HashidRouting;
    use HasTags;
    use InteractsWithMedia;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tst_testimonials';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $casts = [
        'phone' => RawPhoneNumberCast::class.':INTERNATIONAL,PT',
    ];

    public function collection(): BelongsTo
    {
        return $this->belongsTo(TestimonialCollection::class, 'testimonial_collection_id');
    }

    public function pessoa(): BelongsTo
    {
        return $this->belongsTo(Pessoa::class);
    }

    public function reviewer(): BelongsTo
    {
        return $this->belongsTo(Pessoa::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('audio')->singleFile();
        $this->addMediaCollection('images');
    }
}
