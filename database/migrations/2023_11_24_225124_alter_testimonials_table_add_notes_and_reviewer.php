<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('tst_testimonials', function (Blueprint $table) {
            $table->integer('reviewer_id')->unsigned()->nullable();
            $table->foreign('reviewer_id')->references('id')->on('pessoas')->onDelete('set null');

            $table->longText('notes')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table('tst_testimonials', function (Blueprint $table) {
            $table->dropColumn('reviewer_id');
            $table->dropColumn('notes');
        });
    }
};
