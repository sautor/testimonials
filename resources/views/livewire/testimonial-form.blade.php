<div>
    <h1 class="group-page__title">
        {{ $collection->name }}
    </h1>

    @if(count($this->locales) > 1)
        <div class="s-tst-text-xs s-tst-justify-center s-tst-text-gray-500 s-tst-flex s-tst-items-center s-tst-gap-2 -s-tst-mt-8 s-tst-mb-12">
            <span class="far fa-language w-4 h-4"></span>

            @foreach($this->locales as $code => $label)
                &middot;
            <button wire:click="setLocale('{{$code}}')"
                    class=" @if(app()->getLocale() === $code) s-tst-font-bold @else s-tst-underline s-tst-decoration-gray-300 s-tst-decoration-2 @endif ">{{ $label }}</button>
            @endforeach
        </div>
    @endif

    @if ($isSaved)
        <div class="s-tst-max-w-prose s-tst-mx-auto">
            <div class="alert alert--success">
                <p>
                    @lang('testimonials::form.success')
                </p>
            </div>

            <div class="prose lg:prose-lg empty:s-tst-hidden s-tst-mx-auto s-tst-mt-8">
                {!! $collection->thankyou_text !!}
            </div>
        </div>
    @elseif ($currentStep === 0)
        <div class="prose lg:prose-lg empty:s-tst-hidden s-tst-mx-auto s-tst-mb-8">
            {!! $collection->opening_text !!}
        </div>

        <div class="text-center">
            <button type="button" class="button button--primary button--large" wire:click="goToStep1" wire:loading.attr="disabled">
                <span wire:loading>
                    @lang('testimonials::form.loading')
                </span>
                <span wire:loading.remove>
                    @lang('testimonials::form.start')
                </span>
            </button>
        </div>
    @else
        <div class="max-w-prose mx-auto">
            <x-stepper class="mb-8" :current="$currentStep" :step-label="fn ($idx) => __('testimonials::form.step', ['idx' => $idx])">
                <x-stepper.step icon="id-card">
                    @lang('testimonials::form.personal_data')
                </x-stepper.step>
                <x-stepper.step idx="2" :icon="$type === 'audio' ? 'comment-music' : 'comment-lines'">
                    @lang('testimonials::form.testimonial')
                </x-stepper.step>
                @if($collection->accept_images)
                    <x-stepper.step idx="3" icon="images">
                        @lang('testimonials::form.images')
                    </x-stepper.step>
                @endif
                <x-stepper.step :idx="$collection->accept_images ? 4 : 3" icon="check-circle">
                    @lang('testimonials::form.consent')
                </x-stepper.step>
            </x-stepper>

            @if($currentStep === 1)

                <x-forms.input name="name" :label="__('testimonials::form.name')" required :options="['wire:model' => 'name']" />

                <p class="s-tst-mb-2 s-tst-text-sm s-tst-text-gray-500 s-tst-italic">
                    @lang('testimonials::form.contact_message')
                </p>

                <div class="s-tst-grid s-tst-grid-cols-1 md:s-tst-grid-cols-2 s-tst-gap-x-4">
                    <x-forms.input name="email" :label="__('testimonials::form.email')" type="email" :options="['wire:model' => 'email']" />
                    <x-forms.input name="phone" :label="__('testimonials::form.phone')" type="tel" :options="['wire:model' => 'phone']" />
                </div>

                <x-forms.input name="origin" :label="__('testimonials::form.origin')" required :options="['wire:model' => 'origin']" />

                @unless(empty($collection->tags))
                <div class="field">
                    <label class="label">@lang('testimonials::form.tags_label')</label>
                    <div class="s-tst-mt-2 s-tst-space-y-2">
                        @foreach(explode(';', $collection->tags) as $tag)
                            <x-forms.checkbox :id="'tag_'.$tag" name="tags" :label="$tag" no-field :cb-val="$tag" :options="['wire:model' => 'tags']" />
                        @endforeach
                    </div>
                </div>
                @endunless

                <div class="s-tst-flex s-tst-justify-end s-tst-mt-8">
                    <button type="button" class="button" wire:click="goToStep2" wire:loading.attr="disabled">
                        <span wire:loading>
                            @lang('testimonials::form.loading')
                        </span>
                        <span wire:loading.remove>
                            @lang('testimonials::form.next')
                            <span class="far fa-angle-right s-tst-ml-1"></span>
                        </span>
                    </button>
                </div>

            @elseif($currentStep === 2)

                @if($collection->accept_audio)
                    <div class="s-tst-grid s-tst-grid-cols-1 md:s-tst-grid-cols-2 s-tst-gap-x-4 s-tst-mb-8">
                        @foreach(['text', 'audio'] as $value)
                            <div class="field @if($errors->has('type')) field--invalid @endif">
                                <div class="control">
                                    <div class="radio">
                                        <div class="radio__input">
                                            <input type="radio" id="type-{{ $value }}" name="type" value="{{ $value }}" wire:model.live="type">
                                        </div>
                                        <div class="radio__label">
                                            <label for="type-{{ $value }}">
                                                @lang('testimonials::form.testimonial_'.$value)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif

                @if($type === 'text')
                    <div class="field @if($errors->has('content')) field--invalid @endif">
                        <label for="content" class="label">
                            @lang('testimonials::form.content')
                        </label>
                        <div class="s-tst-mt-2 s-tst-grid" x-data="{ content: '' }">
                            <div class="input s-tst-py-2 s-tst-px-3 s-tst-text-base s-tst-whitespace-pre-wrap" style="visibility: hidden; grid-area: 1/1/2/2;" x-html="content">
                            </div>
                            <textarea name="content" id="content" x-model="content" wire:model="content" class="input s-tst-min-h-[10rem] s-tst-resize-none" style="grid-area: 1/1/2/2;"></textarea>
                        </div>
                        @if ($errors->has('content'))
                            <span class="field__help field__help--invalid">
                            {{ $errors->first('content') }}
                        </span>
                        @endif
                    </div>
                @elseif($type === 'audio')
                    <x-forms.file name="audio" :label="__('testimonials::form.audio')" required accept="audio/*,video/mp4" :options="['wire:model' => 'audio']" />
                @endif

                <div class="s-tst-flex s-tst-justify-end s-tst-mt-8">
                    <button type="button" class="button" wire:click="goToStep3" wire:loading.attr="disabled">
                       <span wire:loading>
                            @lang('testimonials::form.loading')
                        </span>
                        <span wire:loading.remove>
                            @lang('testimonials::form.next')
                            <span class="far fa-angle-right s-tst-ml-1"></span>
                        </span>
                    </button>
                </div>

            @elseif($collection->accept_images && $currentStep === 3)

                <x-forms.file name="images" :label="__('testimonials::form.images')" accept="image/*" multiple :options="['wire:model' => 'images']" />

                <div class="s-tst-grid s-tst-grid-cols-4 s-tst-gap-4">
                    @foreach($images as $image)
                        <img src="{{ $image->temporaryUrl() }}" class="s-tst-w-full s-tst-aspect-square s-tst-rounded s-tst-object-cover s-tst-border s-tst-border-gray-300" >
                    @endforeach
                </div>

                <div class="s-tst-flex s-tst-justify-end s-tst-mt-8">
                    <button type="button" class="button" wire:click="goToStep4" wire:loading.attr="disabled">
                        <span wire:loading>
                            @lang('testimonials::form.loading')
                        </span>
                        <span wire:loading.remove>
                            @lang('testimonials::form.next')
                            <span class="far fa-angle-right s-tst-ml-1"></span>
                        </span>
                    </button>
                </div>
            @elseif($currentStep === $collection->accept_images ? 4 : 3)

                <div class="prose empty:s-tst-hidden s-tst-mx-auto s-tst-mb-8">
                    {!! $collection->disclaimer !!}
                </div>

                <x-forms.checkbox name="consent" :label="__('testimonials::form.consent_message', ['group' => $collection->grupo->nome, 'app' => config('app.name')])" :value="isset($collection) ? $collection->accept_audio : null" :options="['wire:model' => 'consent']" />

                <div class="s-tst-flex s-tst-justify-end s-tst-mt-8">
                    <button type="button" class="button button--primary" wire:click="save" wire:loading.attr="disabled">
                        <span wire:loading>
                            @lang('testimonials::form.sending')
                        </span>
                        <span wire:loading.remove>
                            @lang('testimonials::form.send')
                        </span>
                    </button>
                </div>
            @endif

        </div>
    @endif

</div>
