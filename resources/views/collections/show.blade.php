@extends('layouts.group-admin')

@section('group-content')
    <div class="group-page__header">
        <h1 class="group-page__header__title">
            {{ $collection->name }}
            <small>{{ $collection->testimonials_count }} testemunhos</small>
        </h1>

        <div class="group-page__header__actions">
            <a href="{{ $grupo->route('testimonials.collections.edit', $collection) }}" class="button button--responsive">
                <span class="fas fa-pencil"></span>
                Editar
            </a>
        </div>
    </div>


    @include('testimonials::testimonials.list-component')

    {!! $testimonials->links() !!}

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/testimonials/styles.css') }}">
@endpush
