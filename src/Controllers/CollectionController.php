<?php

namespace Sautor\Testimonials\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;
use Sautor\Testimonials\Models\TestimonialCollection;

class CollectionController extends Controller
{
    public function index(Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $this->authorize('viewAny', TestimonialCollection::class);
        $collections = TestimonialCollection::where('grupo_id', $grupo->id)->paginate(20);

        return view('testimonials::collections.index', compact('grupo', 'collections'));
    }

    public function create(Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $this->authorize('create', TestimonialCollection::class);

        return view('testimonials::collections.create', compact('grupo'));
    }

    public function store(Request $request, Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $this->authorize('create', TestimonialCollection::class);

        $this->validate($request, [
            'name' => 'required',
            'open_at' => 'date|nullable',
            'close_at' => 'date|nullable',
            'opening_text' => 'nullable',
            'disclaimer' => 'nullable',
            'thankyou_text' => 'nullable',
            'accept_audio' => 'boolean|nullable',
            'accept_images' => 'boolean|nullable',
            'banner' => 'file|image|nullable',
        ]);

        $collection = new TestimonialCollection($request->except(['banner', 'accept_audio', 'accept_images', 'tags']));
        $collection->accept_audio = $request->has('accept_audio');
        $collection->accept_images = $request->has('accept_images');
        $collection->tags = empty($request->get('tags')) ? null : trim(implode(';', array_map(fn ($item) => trim($item), explode(';', $request->get('tags')))), ';');
        $collection->grupo_id = $grupo->id;
        $collection->save();

        if ($request->hasFile('banner')) {
            if ($request->file('banner')->isValid()) {
                $collection->addMediaFromRequest('banner')->toMediaCollection('banner');
            }
        }

        Notification::make()
            ->title('Coleção criada com sucesso.')
            ->success()
            ->send();

        return redirect($grupo->route('testimonials.collections.index'));
    }

    public function show(Grupo $grupo, TestimonialCollection $collection)
    {
        $this->authorize('view', $collection);
        $testimonials = $collection->testimonials()->paginate(20);
        $collection->loadCount('testimonials');

        return view('testimonials::collections.show', compact('grupo', 'collection', 'testimonials'));
    }

    public function edit(Grupo $grupo, TestimonialCollection $collection)
    {
        $this->authorize('update', $collection);

        return view('testimonials::collections.edit', compact('grupo', 'collection'));
    }

    public function update(Request $request, Grupo $grupo, TestimonialCollection $collection)
    {
        $this->authorize('update', $collection);
        $this->validate($request, [
            'name' => 'required',
            'open_at' => 'date|nullable',
            'close_at' => 'date|nullable',
            'opening_text' => 'nullable',
            'disclaimer' => 'nullable',
            'thankyou_text' => 'nullable',
            'accept_audio' => 'boolean|nullable',
            'accept_images' => 'boolean|nullable',
            'banner' => 'file|image|nullable',
        ]);

        $collection->fill($request->except(['banner', 'accept_audio', 'accept_images', 'tags']));

        $collection->accept_audio = $request->has('accept_audio');
        $collection->accept_images = $request->has('accept_images');
        $collection->tags = empty($request->get('tags')) ? null : trim(implode(';', array_map(fn ($item) => trim($item), explode(';', $request->get('tags')))), ';');
        $collection->save();

        if ($request->hasFile('banner')) {
            if ($request->file('banner')->isValid()) {
                $collection->addMediaFromRequest('banner')->toMediaCollection('banner');
            }
        }

        Notification::make()
            ->title('Coleção editada com sucesso.')
            ->success()
            ->send();

        return redirect($grupo->route('testimonials.collections.index'));
    }

    public function destroy(Grupo $grupo, TestimonialCollection $collection)
    {
        $this->authorize('delete', $collection);

        $collection->delete();

        Notification::make()
            ->title('Coleção de testemunhos eliminada com sucesso.')
            ->success()
            ->send();

        return redirect($grupo->route('testimonials.collections.index'));
    }
}
