<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Sautor\Testimonials\Models\TestimonialCollection;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('tst_testimonials', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('origin');
            $table->longText('content')->nullable();
            $table->longText('reviewed_content')->nullable();

            $table->integer('pessoa_id')->unsigned()->nullable();
            $table->foreign('pessoa_id')->references('id')->on('pessoas')->onDelete('cascade');

            $table->foreignIdFor(TestimonialCollection::class)->nullable()->constrained(table: 'tst_testimonials_collections');

            $table->ipAddress('ip')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down(): void
    {
        Schema::dropIfExists('tst_testimonials');
    }
};
