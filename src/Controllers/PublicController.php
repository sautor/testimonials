<?php

namespace Sautor\Testimonials\Controllers;

use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;
use Sautor\Testimonials\Models\TestimonialCollection;

class PublicController extends Controller
{
    public function index(Grupo $grupo)
    {
        $collections = TestimonialCollection::where('grupo_id', $grupo->id)->open()->get();
        if ($collections->count() === 1) {
            return redirect($grupo->route('testimonials.public.create', $collections->first()));
        }

        return view('testimonials::public.index', compact('grupo', 'collections'));
    }

    public function create(Grupo $grupo, TestimonialCollection $collection)
    {
        return view('testimonials::public.create', compact('grupo', 'collection'));
    }
}
