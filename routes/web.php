<?php

use Sautor\Testimonials\Controllers\CollectionController;
use Sautor\Testimonials\Controllers\PublicController;
use Sautor\Testimonials\Controllers\TestimonialController;

Sautor\groupRoute(function() {
    Route::middleware('addon:testimonials')->name('testimonials.')->prefix('testemunhos')->group(function() {
        Route::get('/', [PublicController::class, 'index'])->name('public.index');

        Route::resource('collections', CollectionController::class)->middleware('auth');

        Route::get('{collection}', [PublicController::class, 'create'])->name('public.create');

        Route::get('testimonials/{testimonial}', [TestimonialController::class, 'show'])->middleware('auth')->name('testimonials.show');
        Route::get('testimonials/{testimonial}/edit', [TestimonialController::class, 'edit'])->middleware('auth')->name('testimonials.edit');
        Route::put('testimonials/{testimonial}', [TestimonialController::class, 'update'])->middleware('auth')->name('testimonials.update');

    });
});
