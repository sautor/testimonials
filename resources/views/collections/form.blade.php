@php($locales = ['pt' => 'Português', 'es' => 'Espanhol', 'en' => 'Inglês'])

<div class="s-tst-grid s-tst-grid-cols-1 md:s-tst-grid-cols-2 s-tst-gap-x-8">
    <x-forms.input name="open_at" label="Abrir a" :value="isset($collection) ? $collection->open_at : null"
                   type="datetime-local" :scope="$scope"/>
    <x-forms.input name="close_at" label="Fechar a" :value="isset($collection) ? $collection->close_at : null"
                   type="datetime-local" :scope="$scope"/>

    <div class="lg:s-tst-col-span-2">
        <x-forms.input name="tags" label="Etiquetas" :value="isset($collection) ? $collection->tags : null" hint="Escrever as etiquetas separadas por ;" :scope="$scope" />
    </div>

    <div>
        <h4 class="title title--xs s-tst-mb-3">Línguas</h4>
        @foreach($locales as $code => $locale)
            <x-forms.checkbox name="languages[]" :id="'languages['.$code.']'" :label="$locale" :value="isset($collection) ? isset($collection->languages) && in_array($code, $collection->languages) : null" :cb-val="$code" :scope="$scope" />
        @endforeach
    </div>

    <div>
        <h4 class="title title--xs s-tst-mb-3">Outras opções</h4>
        <x-forms.checkbox name="accept_audio" label="Aceitar áudio" :value="isset($collection) ? $collection->accept_audio : null" :scope="$scope" />
        <x-forms.checkbox name="accept_images" label="Aceitar imagens" :value="isset($collection) ? $collection->accept_images : null" :scope="$scope" />
        <x-forms.file name="banner" label="Banner" accept=".png,.jpg,.jpeg"
                      rules="ext:png,jpg,jpeg" :scope="$scope" />
    </div>
</div>

@foreach($locales as $code => $locale)
    <h3 class="title title--sm s-tst-my-4">{{ $locale }}</h3>

    <div class="s-tst-grid s-tst-grid-cols-1 md:s-tst-grid-cols-2 s-tst-gap-x-8">
        <x-forms.input
            :name="'name['.$code.']'"
            label="Nome"
            :value="isset($collection) ? $collection->getTranslation('name', $code, false) : null"
            required
            :scope="$scope"
        />
        @foreach(['opening_text' => 'Texto de abertura', 'disclaimer' => 'Aviso', 'thankyou_text' => 'Mensagem de agradecimento'] as $key => $label)
        <div class="field">
            <label class="label">{{ $label }}</label>
            <div class="control">
                <small-content-editor name="{{ $key.'['.$code.']' }}"
                                      value="{{ isset($collection) ? $collection->getTranslation($key, $code, false) : null }}"
                                      simple></small-content-editor>
            </div>
        </div>
        @endforeach
    </div>
@endforeach
