<?php

namespace Sautor\Testimonials\Livewire;

use Filament\Notifications\Notification;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\Attributes\Computed;
use Sautor\Testimonials\Models\Testimonial;
use Sautor\Testimonials\Models\TestimonialCollection;

class TestimonialForm extends Component
{
    use WithFileUploads;

    public TestimonialCollection $collection;

    public string $locale;

    public $currentStep = 1;

    public $name;

    public $email;

    public $phone;

    public $origin;

    public $tags = [];

    public $type;

    public $content;

    public $audio;

    public $images = [];

    public $consent;

    public $isSaved = false;

    #[Computed]
    public function locales()
    {
        $locales = ['pt' => 'Português'];

        if (!empty($this->collection->languages)) {
            $other_locales = ['es' => 'Español', 'en' => 'English'];
            foreach (array_keys($other_locales) as $locale) {
                if (in_array($locale, $this->collection->languages)) {
                    $locales[$locale] = $other_locales[$locale];
                }
            }
        }

        return $locales;
    }

    public function mount($collection)
    {
        $this->locale = $this->get_browser_language();
        app()->setLocale($this->locale);

        $this->collection = $collection;
        if (! empty($this->collection->opening_text)) {
            $this->currentStep = 0;
        }
        if (! $this->collection->accept_audio) {
            $this->type = 'text';
        }
    }

    private function get_browser_language()
    {
        $available = array_keys($this->locales);

        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $langs = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

            foreach ($langs as $lang) {
                $lang = substr($lang, 0, 2);
                if (in_array($lang, $available)) {
                    return $lang;
                }
            }
        }

        return 'pt';
    }

    public function goToStep1()
    {
        $this->currentStep = 1;
    }

    public function goToStep2()
    {
        $this->validate([
            'name' => 'required',
            'email' => 'email|nullable',
            'phone' => 'phone:INTERNATIONAL,PT|nullable',
            'origin' => 'required',
        ], [], [
            'name' => __('testimonials::form.name'),
            'email' => __('testimonials::form.email'),
            'phone' => __('testimonials::form.phone'),
            'origin' => __('testimonials::form.origin'),
        ]);

        $this->currentStep = 2;
    }

    public function goToStep3()
    {
        $this->validate($this->type === 'audio' ? [
            'audio' => 'file',
        ] : [
            'type' => 'required',
            'content' => 'required',
        ], [], [
            'audio' => __('testimonials::form.audio'),
            'content' => __('testimonials::form.content'),
        ]);

        $this->currentStep = 3;
    }

    public function goToStep4()
    {
        $this->validate([
            'images.*' => 'image',
        ], [], [
            'images' => __('testimonials::form.images'),
        ]);

        $this->currentStep = 4;
    }

    public function save()
    {
        $this->validate([
            'consent' => 'accepted',
        ], [], [
            'consent' => __('testimonials::form.consent'),
        ]);

        try {
            \DB::transaction(function () {
                $testimonial = Testimonial::create([
                    'name' => $this->name,
                    'email' => $this->email,
                    'phone' => $this->phone,
                    'origin' => $this->origin,
                    'content' => $this->content,
                    'testimonial_collection_id' => $this->collection->id,
                    'ip' => request()->ip(),
                    'pessoa_id' => \Auth::id(),
                ]);

                if (! empty($this->tags)) {
                    $testimonial->syncTagsWithType($this->tags, $this->collection->tagType);
                }

                if (! empty($this->audio)) {
                    $testimonial->addMedia($this->audio)->toMediaCollection('audio');
                }

                foreach ($this->images as $image) {
                    $testimonial->addMedia($image)->toMediaCollection('images');
                }

                $this->isSaved = true;
            });
        } catch (\Throwable $e) {
            Notification::make()
                ->title(__('testimonials::form.error.title'))
                ->body(__('testimonials::form.error.body'))
                ->danger()
                ->send();
        }
    }

    public function hydrate()
    {
        app()->setLocale($this->locale);
    }

    public function render()
    {
        return view('testimonials::livewire.testimonial-form');
    }

    public function setLocale($code)
    {
        $this->locale = $code;
        app()->setLocale($code);
    }
}
