<?php

return [
    'step' => 'Paso :idx',
    'success' => 'Testimonio enviado con éxito.',
    'loading' => 'Cargando...',
    'start' => 'Empezar',
    'personal_data' => 'Datos personales',
    'testimonial' => 'Testimonio',
    'images' => 'Fotos',
    'consent' => 'Consentimiento',
    'name' => 'Nombre',
    'contact_message' => 'Los campos de contacto a continuación son opcionales y solo se usarán si es necesario.',
    'email' => 'Dirección de correo electrónico',
    'phone' => 'Número de teléfono',
    'origin' => 'Parship y/o grupos a los que pertenece',
    'tags_label' => 'Elija todo lo que corresponda',
    'next' => 'Siguiente',
    'testimonial_text' => 'Testimonio escrito',
    'testimonial_audio' => 'Testimonio de audio',
    'content' => 'Cuerpo del testimonio',
    'audio' => 'Archivo de audio',
    'consent_message' => 'Consiento el uso de este testimonio por :group de :app.',
    'send' => 'Enviar',
    'sending' => 'Enviando...',
    'error' => [
        'title' => 'Ocurrió un error',
        'body' => 'Por favor, inténtelo de nuevo.'
    ]
];
