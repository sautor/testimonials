@extends('layouts.group-admin')

@section('group-content')
    <div class="group-page__header">
        <h1 class="group-page__header__title">
            Testemunho #{{ $testimonial->id }}
            <small>{{ $testimonial->collection->name }}</small>
        </h1>

        <div class="group-page__header__actions">
            <a href="{{ $grupo->route('testimonials.testimonials.edit', $testimonial) }}" class="button button--responsive">
                <span class="far fa-comment-check"></span>
                Rever
            </a>
        </div>
    </div>

    <div class="people-show__tab__card">
        <p class="people-show__item">
            <span class="people-show__item__label">Nome</span>
            {{ $testimonial->name }}
        </p>
        <div class="s-tst-grid md:s-tst-grid-cols-2 s-tst-gap-6">
            <p class="people-show__item">
                <span class="people-show__item__label">Endereço de email</span>
                @empty($testimonial->email)
                    <span class="people-show__item__empty">(não especificado)</span>
                @else
                    {{ $testimonial->email }}
                @endempty
            </p>
            <p class="people-show__item">
                <span class="people-show__item__label">Número de telefone</span>
                @empty($testimonial->phone)
                    <span class="people-show__item__empty">(não especificado)</span>
                @else
                    {{ $testimonial->phone->formatForCountry('PT') }}
                @endempty
            </p>
        </div>
        <p class="people-show__item">
            <span class="people-show__item__label">Paróquia e/ou grupos</span>
            {{ $testimonial->origin }}
        </p>
        <p class="people-show__item">
            <span class="people-show__item__label">Etiquetas</span>
            @if($testimonial->tagsWithType($testimonial->collection->tagType)->isEmpty())
                <span class="people-show__item__empty">(não especificado)</span>
            @else
                {{ $testimonial->tagsWithType($testimonial->collection->tagType)->pluck('name')->join(', ') }}
            @endif
        </p>
        <div class="s-tst-grid md:s-tst-grid-cols-2 s-tst-gap-6">
            <div class="people-show__item">
                <span class="people-show__item__label">Carregado por</span>
                @empty($testimonial->pessoa)
                    <span class="people-show__item__empty">IP: {{ $testimonial->ip }} (sem sessão iniciada)</span>
                @else
                    <x-people.show :person="$testimonial->pessoa" :meta="'IP:'.$testimonial->ip" />
                @endempty
            </div>
            <div class="people-show__item">
                <span class="people-show__item__label">Revisor</span>
                @empty($testimonial->reviewer)
                    <span class="people-show__item__empty">(sem revisor associado)</span>
                @else
                    <x-people.show :person="$testimonial->reviewer" />
                @endempty
            </div>
        </div>
    </div>

    <div class="s-tst-grid lg:s-tst-grid-cols-2 s-tst-gap-6">

        <div>
            @if($testimonial->hasMedia('audio'))
                <header class="people-show__tab__header">
                    <h2 class="people-show__tab__title">Ficheiro áudio</h2>
                </header>
                <div class="people-show__tab__card">
                    <audio controls src="{{ $testimonial->getFirstMediaUrl('audio') }}"></audio>
                </div>
            @else
                <header class="people-show__tab__header">
                    <h2 class="people-show__tab__title">Conteúdo</h2>
                </header>
                <div class="people-show__tab__card">
                    <div class="prose whitespace-pre-line">{!! $testimonial->content !!}</div>
                </div>
            @endif
        </div>

        <div>
            <header class="people-show__tab__header">
                <h2 class="people-show__tab__title">Conteúdo revisto</h2>
            </header>
            <div class="people-show__tab__card">
                <div class="prose s-tst-whitespace-pre-line">{!! $testimonial->reviewed_content !!}</div>
            </div>
        </div>

    </div>

    @unless(empty($testimonial->notes))
        <header class="people-show__tab__header">
            <h2 class="people-show__tab__title">Notas</h2>
        </header>
            <div class="prose s-tst-whitespace-pre-line s-tst-mb-8 s-tst-italic s-tst-font-accent s-tst-text-gray-700">{!! $testimonial->notes !!}</div>
    @endunless

    @if($testimonial->hasMedia('images'))
        <header class="people-show__tab__header">
            <h2 class="people-show__tab__title">Fotografias</h2>
        </header>

        <div class="s-tst-grid s-tst-grid-cols-3 md:s-tst-grid-cols-4 lg:s-tst-grid-cols-5 xl:s-tst-grid-cols-6 s-tst-gap-4">
            @foreach($testimonial->getMedia('images') as $image)
                <a href="{{ $image->getUrl() }}" target="_blank">
                    <img src="{{ $image->getUrl() }}" class="s-tst-w-full s-tst-aspect-square s-tst-rounded s-tst-object-cover s-tst-border s-tst-border-gray-300" >
                </a>
            @endforeach
        </div>
    @endif

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/testimonials/styles.css') }}">
@endpush
