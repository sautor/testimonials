# Sautor Testimonials

Extensão de testemunhos para o sistema Sautor.

## Instalação

Instalar via Composer:

````sh
composer require sautor/testimonials
````

Para correr a migração:
````sh
php artisan migrate
````
