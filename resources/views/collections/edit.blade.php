@extends('layouts.group-admin')

@section('group-content')
    <header class="group-page__header">
        <h1 class="group-page__header__title">
            Editar coleção de testemunhos
        </h1>
    </header>

    @php($scope = 'edit-collection')
    <form action="{{ $grupo->route('testimonials.collections.update', $collection) }}" method="POST" data-vv-scope="{{ $scope }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('testimonials::collections.form')

        <div class="sautor-addon__actions">
            <button type="submit" class="button button--primary">Editar</button>
            @can('delete', $collection)
                <button type="button" class="button button--danger-light" @click.prevent="openModal('deleteModal')">
                    <span class="far fa-trash mr-2"></span>
                    Eliminar
                </button>
            @endcan
        </div>
    </form>

    @can('delete', $collection)
        <modal id="deleteModal">
            <div class="modal__body modal__confirmation">
                <div class="modal__confirmation__icon">
                    <span class="fas fa-exclamation"></span>
                </div>
                <div class="modal__confirmation__content">
                    <h3 class="modal__confirmation__title">Eliminar coleção de testemunhos</h3>
                    <p class="modal__confirmation__text">
                        Tem a certeza que quer eliminar a coleção <strong>{{ $collection->name }}</strong>?
                    </p>
                </div>
            </div>
            <form class="modal__footer" method="POST" action="{{ $collection->grupo->route('testimonials.collections.destroy', $collection) }}">
                @csrf
                @method('DELETE')
                <button type="submit" class="button button--danger">Eliminar</button>
                <button type="button" class="button" @click.prevent="closeModal('deleteModal')">Cancelar</button>
            </form>
        </modal>
    @endcan
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/testimonials/styles.css') }}">
@endpush
