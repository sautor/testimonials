@extends('layouts.group-admin')

@section('group-content')
    <div class="group-page__header">
        <h1 class="group-page__header__title">
            Coleções de testemunhos
        </h1>

        <div class="group-page__header__actions">
            <a href="{{ $grupo->route('testimonials.collections.create') }}" class="button button--responsive">
                <span class="far fa-comment-plus"></span>
                Nova coleção
            </a>
        </div>
    </div>

    @include('testimonials::collections.list-component', ['actions' => fn ($tc) => [
        ['label' => 'Editar', 'icon' => 'fas fa-pencil', 'link' => $tc->grupo->route('testimonials.collections.edit', $tc)]
    ]])

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/testimonials/styles.css') }}">
@endpush
