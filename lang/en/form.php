<?php

return [
    'step' => 'Step :idx',
    'success' => 'Testimonial sent successfully.',
    'loading' => 'Loading...',
    'start' => 'Start',
    'personal_data' => 'Personal data',
    'testimonial' => 'Testimonial',
    'images' => 'Photos',
    'consent' => 'Consent',
    'name' => 'Name',
    'contact_message' => 'The contact fields below are optional and will only be used if needed.',
    'email' => 'Email address',
    'phone' => 'Phone number',
    'origin' => 'Parship and/or groups you belong to',
    'tags_label' => 'Choose all that apply',
    'next' => 'Next',
    'testimonial_text' => 'Text testimonial',
    'testimonial_audio' => 'Audio testimonial',
    'content' => 'Testimonial body',
    'audio' => 'Audio file',
    'consent_message' => 'I consent the use of this testimonial by :group of :app.',
    'send' => 'Send',
    'sending' => 'Sending...',
    'error' => [
        'title' => 'An error occurred',
        'body' => 'Please try again.'
    ]
];
