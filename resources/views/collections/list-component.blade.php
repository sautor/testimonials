@unless($collections->isEmpty())
    <section class="sautor-list sheets-list">
        @foreach($collections as $collection)
            <div class="sautor-list__item">
                <div class="sautor-list__item__logo">
                    <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
                        <span class="fad fa-comments fa-swap-opacity"></span>
                    </span>
                </div>
                <a href="{{ $collection->grupo->route('testimonials.collections.show', $collection) }}" class="sautor-list__item__data">
                    <p class="sautor-list__item__name">
                        {{ $collection->name }}
                    </p>
                    <p class="sautor-list__item__meta">
                        {{ $collection->testimonials->count() }} testemunhos
                        @if($collection->isOpen)
                            <span class="badge badge--success">Aberto</span>
                        @elseif($collection->isClosed)
                            <span class="badge badge--danger">Fechado</span>
                        @else
                            <span class="badge">Por abrir</span>
                        @endif
                    </p>
                </a>

                <div class="sautor-list__item__actions">
                    @unless(empty($actions) or empty($a = $actions($collection)))
                        @foreach($a as $action)
                            <a title="{{ $action['label'] }}"
                               @if(!empty($action['link']))
                                   href="{{ $action['link'] }}"
                               @elseif(!empty($action['modal']))
                                   href="#" data-toggle="modal" data-target="#{{ $action['modal'] }}"
                                    @endif
                            >
                                <span class="{{ $action['icon'] }}"></span>
                            </a>
                        @endforeach
                    @endunless
                    <a title="Ver" href="{{ $collection->grupo->route('testimonials.collections.show', $collection) }}">
                        <span class="far fa-angle-right"></span>
                    </a>
                </div>
            </div>
        @endforeach
    </section>
@else
    <section class="sautor-list--empty tags-list--empty">
        {{ empty($empty_message) ? 'Não existem coleções.' : $empty_message }}
    </section>
@endunless
